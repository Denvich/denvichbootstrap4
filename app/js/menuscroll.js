
$(document).ready(function () {

    function Scroll_block(){

        var scroll_top = $(document).scrollTop();

        $("#top-menu .navbar-nav a").each(function(){
            var hash = $(this).attr("href");
            var target = $(hash);
            if (target.position().top <= scroll_top && target.position().top + target.outerHeight() > scroll_top) {
                $(".navbar-nav a").parent().removeClass("active");
                $(this).parent().addClass("active");
            } else {
                $(this).parent().removeClass("active");
            }
        });
    }
 
    $(document).on("scroll", Scroll_block);

});
